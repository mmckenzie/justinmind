(function(window, undefined) {
var dictionary = {
"46392db2-fb4d-4766-bf95-65e24d57bd29": "Registration",
"92e6eb09-7b00-43cf-bb40-b527f90df5cd": "Choose Registered or Not",
"0741b213-55dc-4884-a7d8-83bfcdef9df6": "Scan a finger",
"0bce616d-20f7-4caa-b337-6dcdb11e298b": "Fingerprint Sensor",
"d12245cc-1680-458d-89dd-4f0d7fb22724": "RemitOne Login Page",
"87db3cf7-6bd4-40c3-b29c-45680fb11462": "960 grid - 16 columns",
"e5f958a4-53ae-426e-8c05-2f7d8e00b762": "960 grid - 12 columns",
"f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
"bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
};

var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
window.lookUpURL = function(fragment) {
var matches = uriRE.exec(fragment || "") || [],
folder = matches[2] || "",
canvas = matches[3] || "",
name, url;
if(dictionary.hasOwnProperty(canvas)) { /* search by name */
url = folder + "/" + canvas;
}
return url;
};

window.lookUpName = function(fragment) {
var matches = uriRE.exec(fragment || "") || [],
folder = matches[2] || "",
canvas = matches[3] || "",
name, canvasName;
if(dictionary.hasOwnProperty(canvas)) { /* search by name */
canvasName = dictionary[canvas];
}
return canvasName;
};
})(window);