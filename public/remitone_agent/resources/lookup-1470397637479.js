(function(window, undefined) {
var dictionary = {
"1cd00dca-b59d-48f1-be5a-86603599dd23": "Finger not found",
"0741b213-55dc-4884-a7d8-83bfcdef9df6": "Scan a finger",
"c8b3f01b-f583-416b-9ca2-78b13659ba18": "Choose Login finger",
"c5fe8372-62a4-4724-a32f-9865d60f4d97": "Logged In Page",
"f8d40fe4-0f57-4280-a418-61cb5f123cb4": "Are you sure",
"46392db2-fb4d-4766-bf95-65e24d57bd29": "Registration",
"5ba579d0-3c7a-4868-9f5d-fdb0da98542e": "AR2 Select previous login finger",
"c83341dd-3c46-4423-a898-74428420d5d7": "AR1 Choose Login finger",
"f040a0f6-7146-4615-8674-900439495fcf": "Fingerprint Sensor Not Found",
"a728a01f-0cac-4a8e-9f25-a17f858cfefc": "Select Finger",
"7d3ceec1-924f-40be-b65a-2bfd51461d15": "Already registered",
"f0526f86-a6f0-4581-9620-08904ae465a3": "Confirm Login finger",
"fef92202-45ae-41a3-923e-732515a19b9e": "Bad swipe",
"14022732-b1d2-4fbc-9b69-89da7a2cf05e": "AR1 Scan chosen fingers",
"8d9c8de7-3f88-40d6-8f07-17b1c738dc62": "Access Recovery 2",
"0c313c9e-b3fc-4b82-8f6e-5f35b55440c6": "Access Recovery 1",
"7943a7a3-136d-431c-9c9f-bd214f696c55": "AR2 Scan chosen fingers",
"0bce616d-20f7-4caa-b337-6dcdb11e298b": "Pipa Software not detected",
"b3b84479-92b1-45c3-a3f1-cbc986288f60": "AR1 Confirm Login finger",
"c8ed09e6-75e0-4089-9373-9c35ecb8120c": "Login",
"8ef34393-fe03-4246-9bba-93b6ea02ac48": "Select another finger",
"92e6eb09-7b00-43cf-bb40-b527f90df5cd": "Choices",
"d12245cc-1680-458d-89dd-4f0d7fb22724": "RemitOne Login Page",
"71a6f0b3-a968-4e7c-a967-261e2b94e8df": "AR2 Confirm Login finger",
"3683a3cd-9ec3-41bb-8762-d2a55fa58d0a": "AR1 Select previous login finger",
"24971fda-e2c0-4f56-9c0d-13c6b2491bbb": "Scan another finger",
"87db3cf7-6bd4-40c3-b29c-45680fb11462": "960 grid - 16 columns",
"e5f958a4-53ae-426e-8c05-2f7d8e00b762": "960 grid - 12 columns",
"f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
"bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
};

var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
window.lookUpURL = function(fragment) {
var matches = uriRE.exec(fragment || "") || [],
folder = matches[2] || "",
canvas = matches[3] || "",
name, url;
if(dictionary.hasOwnProperty(canvas)) { /* search by name */
url = folder + "/" + canvas;
}
return url;
};

window.lookUpName = function(fragment) {
var matches = uriRE.exec(fragment || "") || [],
folder = matches[2] || "",
canvas = matches[3] || "",
name, canvasName;
if(dictionary.hasOwnProperty(canvas)) { /* search by name */
canvasName = dictionary[canvas];
}
return canvasName;
};
})(window);